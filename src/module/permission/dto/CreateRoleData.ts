/**
 * @package module.permission.dto
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class CreateRoleData
 */
export class CreateRoleData {
    title: string;
    name: string;
    persistence?: boolean;
}
