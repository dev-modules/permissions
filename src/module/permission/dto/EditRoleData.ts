/**
 * @package module.permission.dto
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class EditRoleData
 */
export class EditRoleData {
    id: number;
    title?: string;
    name?: string;
}
