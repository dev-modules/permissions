/**
 * @package module.permission.interface
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @interface PermissionInterface
 */
export interface PermissionInterface extends Object {
    name?: string;
    permission: string;
}
